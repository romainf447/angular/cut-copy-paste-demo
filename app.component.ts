
import { Component } from '@angular/core';    
    
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
    youCopied:boolean = false;
    youCut:boolean = false;
    youPasted:boolean = false;
    onCopy(event:any) { this.youCopied = true; console.log("Copied ! ");}
    onCut(event:any) {  this.youCut = true; console.log("Cut !");}
    onPaste(event:any) {  this.youPasted = true; console.log("Pasted !");}

    counterCut:number = 0;
    counterCopy:number = 0;
    counterPaste:number = 0;
    countCopy() { this.counterCopy += 1; console.log("Copied ! ");}
    countCut() {  this.counterCut += 1; console.log("Cut !");}
    countPaste() {  this.counterPaste += 1; console.log("Pasted !");}
}
